import assert from 'assert';
import Schema from '@perfect-data/schema';
import matchPlugin from '../src/match';

describe('Testing Match Validator', () => {

  Schema.use(matchPlugin);

  it('should extend validators', () => {
    [
      /test/,
      new RegExp('test'),
      'test'
    ].forEach(match => {
      const schema = new Schema({
        foo: {
          type: String,
          match
        },
        dummy: Number
      });
      const context = schema.createContext();

      context.validate({ foo: 'test' });
      assert.ok( context.isValid() );

      context.validate({ foo: 'invalid' });
      assert.ok( !context.isValid() );
      assert.deepStrictEqual( context.getMessages(), { foo: 'mismatch' });
    });
  });


  it('should fail if custom is not a function', () => {
    [
      undefined, null, NaN, Infinity, -1, 0, 1,
      {}, [], new Date(), () => {}
    ].forEach(match => assert.throws(() => new Schema({ foo: { type: String, match }}) ));
  });


});
