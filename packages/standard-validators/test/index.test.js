import assert from 'assert';
import * as index from '../src/index';

describe('Testing index', () => {

   it('should export validators', () => {

      assert.ok(typeof index.allowedValuesValidator === 'function');
      assert.ok(typeof index.restrictedValuesValidator === 'function');
      assert.ok(typeof index.matchValidator === 'function');
      assert.ok(typeof index.customValidator === 'function');

   });

});
