# Perfect Schema Standard Validatiors Plugin

Provide standard validation for schemas.


## Install

```
npm i -S @perfect-schema/standard-validators
```


## Usage

```js
import Schema from '@perfect-data/schema';
import { 
  matchValidator,
  restrictedValuesValidator
} from '@perfect-data/schema-standard-validators';


Schema.use(matchValidator);
Schema.use(restrictedValuesValidator);

const schema = new Schema({
  foo: {
    type: String,
    match: /[a-z0-9]+/i
  },
  bar: {
    type: Number,
    restrictedValues: [2, 3, 5, 7, 11, 13, 17, 19]
  }
});
```


## Documentation

*Coming soon...*


## license

MIT
