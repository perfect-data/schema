import assert from 'assert';

import SchemaType, { TypeProvider, isType, getType } from '../../src/types';


describe('Testing type registry map', () => {

  it('should have all types', () => {

    assert.ok( SchemaType.Any );
    assert.ok( SchemaType.AnyOf );
    assert.ok( SchemaType.ArrayOf );
    assert.ok( SchemaType.Integer );

    assert.ok( isType(Array) );
    assert.ok( isType(Boolean) );
    assert.ok( isType(Date) );
    assert.ok( isType(Number) );
    assert.ok( isType(Object) );
    assert.ok( isType(String) );
    
    assert.ok( getType(Array) === SchemaType.Array );
    assert.ok( getType(Boolean) === SchemaType.Boolean );
    assert.ok( getType(Date) === SchemaType.Date );
    assert.ok( getType(Number) === SchemaType.Number );
    assert.ok( getType(Object) === SchemaType.Object );
    assert.ok( getType(String) === SchemaType.String );
  });


  it('should properly map their respective types', () => {
    assert.ok( getType('array') === SchemaType.Array );
    assert.ok( getType('boolean') === SchemaType.Boolean );
    assert.ok( getType('date') === SchemaType.Date );
    assert.ok( getType('number') === SchemaType.Number );
    assert.ok( getType('object') === SchemaType.Object );
    assert.ok( getType('string') === SchemaType.String );
    assert.ok( getType('*') === SchemaType.Any );
  });


  it('should return array types', () => {

    assert.ok( getType([]) === SchemaType.Array );
    assert.ok( getType([String]).$$type === SchemaType.ArrayOf(String).$$type );
    assert.ok( getType([String, Number]).$$type === SchemaType.AnyOf(String, Number).$$type );

  });


  it('should get user type identity', () => {
    const type = {
      $$type: 'test',
      validatorFactory: () => {}
    };

    assert.ok( isType(type) );
    assert.ok( getType(type) === type );
  });


  it('should fail with unknown type', () => {
    [
      undefined, null, false, true, -1, 0, 1,
      {},
      { $$type: 'test' },
      { validatorFactory: () => {} }
    ].forEach(type => assert.ok( getType(type) === undefined ));
  });


  it('should validate TypeProvider', () => {
    assert.throws(() => new TypeProvider());

    class NotImplementedTypeProvider extends TypeProvider {

    }

    class InvalidTypeProvider extends TypeProvider {
      getType() {
        /* whatever, this is not important */
      }
    }

    class CustomTypeProvider extends TypeProvider {
      getType() {
        return {
          $$type: 'test',
          validatorFactory: () => {}
        };
      }
    }

    assert.throws(() => {
      const customType = new NotImplementedTypeProvider();

      getType(customType);
    });

    assert.throws(() => {
      const customType = new InvalidTypeProvider();

      getType(customType);
    });

    assert.doesNotThrow(() => {
      const customType = new CustomTypeProvider();
      const type = getType(customType);

      assert.strictEqual( type.$$type, 'test' );
    });    

  });

});
