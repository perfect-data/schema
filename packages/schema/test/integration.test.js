import assert from 'assert';
import Schema from '../src/schema';
import SchemaType from '../src/types';


describe('Testing integration', () => {

  it('should expose built-in types', () => {
    assert.ok( SchemaType.Any );
    assert.ok( SchemaType.AnyOf );
    assert.ok( SchemaType.ArrayOf );
    assert.ok( SchemaType.Integer );
  });


  it('should create and validate simple schema', () => {

    const itemSchema = new Schema({
      _id: String,
      name: {
        type: String,
        required: true,
        min: 3
      },
      qty: {
        type: Number,
        defaultValue: 0
      },
      price: Number,
      data: SchemaType.Any
    });

    const cartSchema = new Schema({
      items: SchemaType.ArrayOf(itemSchema)
    });


    const item = itemSchema.createModel({
      name: 'foo',
      data: { prop: 'value' }
    });
    const itemContext = itemSchema.createContext();

    const cart = cartSchema.createModel({
      items: [item]
    });
    const cartContext = cartSchema.createContext();


    assert.deepStrictEqual(item, { name: 'foo', qty: 0, data: { prop: 'value' } });
    itemContext.validate(item);
    assert.ok( itemContext.isValid() );

    item.data = true;   // set data to a different type

    assert.deepStrictEqual(item, { name: 'foo', qty: 0, data: true });
    itemContext.validate(item);
    assert.ok( itemContext.isValid() );

    item.qty = /123/;

    assert.deepStrictEqual(item, { name: 'foo', qty: /123/, data: true });
    itemContext.validate(item);
    assert.ok( !itemContext.isValid() );


    assert.ok( cartContext.isValid() );
    cartContext.validate(cart);
    assert.ok( !cartContext.isValid() );
    assert.deepStrictEqual(cartContext.getMessages(), { 'items.0.qty': 'invalidType', 'items': 'invalid' });

    item.qty = 2;

    cartContext.validate(cart);
    assert.ok( cartContext.isValid() );
    assert.deepStrictEqual(cartContext.getMessages(), {});
  });


  it('should recursively back propagate error messages', () => {
    const c = new Schema({ c: String });
    const b = new Schema({ b: c });
    const a = new Schema({ a: b });

    const ctx = a.createContext();

    ctx.validate({ a: { b: { c: true }}});
    assert.deepStrictEqual( ctx.getMessages(), {
      'a': 'invalid',
      'a.b': 'invalid',
      'a.b.c': 'invalidType'
    } );
    assert.ok( !ctx.isValid() );

    ctx.validate({ a: { b: { c: 'ok' }}});
    assert.deepStrictEqual( ctx.getMessages(), {} );
    assert.ok( ctx.isValid() );

    ctx.validate({ a: { b: true }});
    assert.deepStrictEqual( ctx.getMessages(), {
      'a': 'invalid',
      'a.b': 'invalidType',
    } );
    assert.ok( !ctx.isValid() );

    ctx.validate({ a: true });
    assert.deepStrictEqual( ctx.getMessages(), {
      'a': 'invalidType'
    } );
    assert.ok( !ctx.isValid() );

  });


  describe('Testing inline subtypes', () => {

    it('should create anyOf with objects or schema', () => {
      const a = new Schema({
        field: SchemaType.AnyOf(
          String,
          { objField: String },
          new Schema({
            schemaField: String
          })
        )
      });

      const b = new Schema({
        a: SchemaType.ArrayOf(String),
        b: SchemaType.ArrayOf({ subB: String }),
        c: SchemaType.ArrayOf(new Schema({ subC: String })),
      });
    });

  });


});
