# Perfect Data - Schema

Design and validate data using schema


## Install

```
npm i @perfect-data/schema
```

## Usage

```js
import Schema, { SchemaType } from '@perfect-data/schema';

const itemSchema = new Schema({
  _id: String,
  name: {
    type: String,
    required: true,
    minLength: 3
  },
  qty: {
    type: SchemaType.Integer,
    minInteger: 0,
    defaultValue: 0
  },
  price: Number
});

const cartItemSchema = new Schema({
  itemId: String,
  qty: Number
});

const cartSchema = new Schema({
  _id: String,
  userId: String,
  items: SchemaTypes.ArrayOf(cartItemSchema)
})

const context = cartSchema.createContext();
const item = cartSchema.createModel({
  items: [{
    name: 'foo'
  }]
});


if (!context.validate(item)) {
  // context.isValid() == false
  console.error(context.getMessages());
}
```

## Documentation

*Coming soon...*


## license

MIT