import AnyType from './any';
import AnyOfType from './any-of';
import ArrayType from './array';
import ArrayOfType from './array-of';
import BooleanType from './boolean';
import DateType from './date';
import IntegerType from './integer';
import NumberType from './number';
import ObjectType from './object';
import StringType from './string';

const typeMap = [
  AnyType,
  ArrayType,
  BooleanType,
  DateType,
  NumberType,
  ObjectType,
  StringType
].reduce((map, type) => {
  map[String(type.$$type).slice(7, -1)] = type;
  map[type.$$primitive] = type;
  return map;
}, {});

function isType(type) {
  return type && ((type instanceof TypeProvider) || typeMap[type] || (type.$$type && !Array.isArray(type.$$type) && (typeof type.validatorFactory === 'function'))) || false;
}

function getType(type) {
  if (type instanceof TypeProvider) {
    type = type.getType();

    if (!isType(type)) {
      throw new TypeError('TypeProvider does not return valid type');
    }

    return type;
  } else if (Array.isArray(type)) {
    if (!type.length) {
      return ArrayType;
    } else if (type.length === 1) {
      return ArrayOfType( typeMap[type[0]] );
    } else {
      return AnyOfType( ...type.map(t => getType(typeMap[t])) );
    }
  } else if (isType(type)) {
    return typeMap[type] || type;
  }
} 


/**
 * Implement custom types that can be validated
 */
class TypeProvider {
  constructor() {
    if (this.__proto__ === TypeProvider.prototype) {
      throw new Error('Cannot directly create instance of TypeProvider');
    }
  }

  /**
   * Return the type of this object
   * @return {type}
   */
  getType() {
    throw new Error('Not implemented');
  }
}


export default {
  Any: AnyType,
  AnyOf: AnyOfType,
  Array: ArrayType,
  ArrayOf: ArrayOfType,
  Boolean: BooleanType,
  Date: DateType,
  Integer: IntegerType,
  Number: NumberType,
  Object: ObjectType,
  String: StringType
};

export {
  TypeProvider,
  isType, 
  getType
};
