import Schema from './schema';
export { default as SchemaType } from './types';

export default Schema;