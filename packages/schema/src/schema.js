import varValidator from 'var-validator';

import ValidationContext from './context';
import { createModel } from './model';

import { TypeProvider, isType, getType } from './types';


// field validator config
varValidator.enableScope = false;
varValidator.enableBrackets = false;

// schema id counter
let schemaCount = 0;



class Schema extends TypeProvider {

  /**
  Add a plugin to use with new instances of Schema. Added
  plugins do not affect currently instanciated instances.

  @param plugin {Function} a single function receiving the instance
  */
  static use(pluginFactory) {
    const plugin = pluginFactory(Schema)

    if (plugin) {
      if (typeof plugin === 'function') {
        Schema._plugins.push({
          init: plugin
        });
      } else {
        Schema._plugins.push(plugin);
      }
    }

    return Schema;
  }

  /**
  Create a new instance

  @param fields {Object} the fields definition (will be sanitized and normalized)
  @params options {Object} the schema options
  */
  constructor(fields, options = {}) {
    super();

    if (!fields || !Object.keys(fields).length) {
      throw new TypeError('No defined fields');
    } else if (typeof fields !== 'object') {
      throw new TypeError('Invalid fields argument');
    }

    Schema._plugins.forEach(plugin => plugin.preInit && plugin.preInit(this, fields, options));

    Object.defineProperties(this, {
      options: {
        enumerable: true,
        configurable: false,
        writable: false,
        value: options
      },
      fields: {
        enumerable: true,
        configurable: false,
        writable: false,
        value: normalizeValidators(fields, this)
      },
      fieldNames: {
        enumerable: true,
        configurable: false,
        writable: false,
        value: Object.keys(fields)
      },
      _type: {
        enumerable: false,
        configurable: false,
        writable: false,
        value: createType(this)
      },
      _namedContexts: {
        enumerable: false,
        configurable: false,
        writable: false,
        value: {}
      }
    });

    Schema._plugins.forEach(plugin => plugin.init && plugin.init(this));

    this.fieldNames.forEach(field => Object.freeze(this.fields[field]));

    Object.freeze(this.fields);     // no further mods!
    Object.freeze(this.fieldNames); //
  }


  /**
  Return the type for this schema

  @return {type}
  */
  getType() {
    return this._type;
  }


  /**
  Create a new empty model from the fields' default values specification

  @return {Object}
  */
  createModel(data) {
    const model = createModel(this, data);

    Schema._plugins.forEach(plugin => plugin.extendModel && plugin.extendModel(model, this));

    return model;
  }

  /**
  Create a new validation context based on this schema

  @param name {String}   (optional) return a shared context identified by name
  */
  createContext(name) {
    const newContext = !name || !this._namedContexts[name];
    const context = newContext ? new ValidationContext(this) : this._namedContexts[name];

    if (newContext) {
      Schema._plugins.forEach(plugin => plugin.extendContext && plugin.extendContext(context, this));

      if (name) {
        this._namedContexts[name] = context;
      }
    }

    return context;
  }

}


// internal properties
Object.defineProperties(Schema, {
  _plugins: {
    enumerable: false,
    configurable: false,
    writable: false,
    value: []
  },
  _normalizeField: {
    enumerable: false,
    configurable: false,
    writable: false,
    value: normalizeField
  }
});

Object.defineProperties(Schema.prototype, {
  _normalizeField: {
    enumerable: false,
    configurable: false,
    writable: false,
    value: normalizeField
  }
});



/**
Create a type for the given schema

@param schemaType {Schema}
@return {type}
*/
function createType(schemaType) {
  return {
    $$type: Symbol('schema' + (++schemaCount)),
    validatorFactory: (fieldName, field, schema, wrappedValidator) => {
      const {
        required = false,
        nullable = true
      } = field;
      const validatorContext = schemaType.createContext();

      function validator(value, options, context) {
        if (value === undefined) {
          return required ? 'required' : undefined;
        } else if (value === null) {
          return !nullable ? 'isNull' : undefined;
        } else {

          try {
            if (!validatorContext.validate(value)) {
              if (fieldName) {
                const contextMessages = validatorContext.getMessages();

                Object.keys(contextMessages).forEach(subFieldName => context.setMessage(fieldName + '.' + subFieldName, contextMessages[subFieldName]));
              }

              return 'invalid';
            }
          } catch (e) {
            return 'invalidType';
          }
        }

        return wrappedValidator && wrappedValidator(value, options, context);
      }

      validator.context = validatorContext;

      return validator;
    }
  };
}


/**
Sanitize all fields from the given object, make sure that each
key is a valid name, and that each type if a recognized validator

@param fields {object}
@param schema {Schema}
@return {Object}
*/
function normalizeValidators(fields, schema) {
  const fieldNames = Object.keys(fields);

  for (const fieldName of fieldNames) {
    if (!varValidator.isValid(fieldName)) {
      throw new Error('Invalid field name : ' + fieldName);
    }

    const field = fields[fieldName] = normalizeField(fields[fieldName], fieldName);

    field.validator = field.type.validatorFactory(fieldName, field, schema, field.validator);
  }

  return fields;
}


/**
Return an object that is normalized with a valid type property

@param field
@param fieldName {String}   (optional) the field name
@return {Object}
*/
function normalizeField(field, fieldName) {
  if (!field) {
    throw new TypeError('Empty field specification' + (fieldName ? (' for ' + fieldName) : ''));
  }

  if (isType(field)) {
    field = { type: getType(field) };
  } else if (isType(field.type)) {
    field.type = getType(field.type);
  } else if (typeof field === 'object') {
    const typeSchema = new Schema(field);

    field = { type: typeSchema.getType(), schema: typeSchema };
  }

  if (!field.type || !field.type.$$type) {
    throw new TypeError('Invalid field specification' + (fieldName ? (' for ' + fieldName) : ''));
  }

  return field;
}



export default Schema;
